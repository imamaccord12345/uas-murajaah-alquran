const Colors = {
  white: '#FFF',
  purple1: '#189ab0',
  purple2: '#6632BB',
  purple3: '#893FD5',
  grey1: '#F0F4F6',
  grey2: '#0d7a8c',
  grey3: '#c0f2fa',
  line: '#F6F8FA'
};

export default Colors;
